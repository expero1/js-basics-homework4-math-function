/*
  Реалізувати функцію, яка виконуватиме математичні операції з введеними користувачем числами. 
  Завдання має бути виконане на чистому Javascript без використання бібліотек типу jQuery або React.

  Технічні вимоги:

  - Отримати за допомогою модального вікна браузера два числа.
  - Отримати за допомогою модального вікна браузера математичну операцію, 
    яку потрібно виконати. Сюди може бути введено +, -, *, /.
  - Створити функцію, в яку передати два значення та операцію.
  - Вивести у консоль результат виконання функції.


  Необов'язкове завдання підвищеної складності

  Після введення даних додати перевірку їхньої коректності. 
  Якщо користувач не ввів числа, або при вводі вказав не числа, - запитати обидва числа знову 
  (при цьому значенням за замовчуванням для кожної зі змінних має бути введена інформація раніше).
*/

const checkisNumber = (number) => {
  return (typeof number === "string" && number.trim().length) ||
    typeof number === "number"
    ? !isNaN(number)
    : false;
};
function getNumbersAndMathFunctionWithPrompt() {
  let firstNumber = "";
  let secondNumber = "";
  let mathFunction = "";
  const allMathFunctions = ["+", "-", "*", "/"];
  while (true) {
    firstNumber = prompt("Enter first number", firstNumber);
    secondNumber = prompt("Enter second number", secondNumber);
    mathFunction = prompt(
      "Enter one of following math functions: +, -, *, /",
      mathFunction
    );
    if (
      checkisNumber(firstNumber) &&
      checkisNumber(secondNumber) &&
      allMathFunctions.includes(mathFunction)
    )
      return [Number(firstNumber), Number(secondNumber), mathFunction];
  }
}
function calculate(firstNumber, secondNumber, mathFunction) {
  switch (mathFunction) {
    case "+":
      return firstNumber + secondNumber;
    case "-":
      return firstNumber - secondNumber;
    case "*":
      return firstNumber * secondNumber;
    case "/":
      return firstNumber / secondNumber;
  }
}

console.log(calculate(...getNumbersAndMathFunctionWithPrompt()));
